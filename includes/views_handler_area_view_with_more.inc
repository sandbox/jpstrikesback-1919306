<?php

/**
 * @file
 * Definition of views_handler_area_view.
 */

/**
 * Views area handlers. Insert a view inside of an area.
 *
 * @ingroup views_area_handlers
 */
class views_handler_area_view_with_more extends views_handler_area {

  function option_definition() {
    $options = parent::option_definition();

    $options['view_to_insert'] = array('default' => '');
    $options['inherit_arguments'] = array('default' => FALSE, 'bool' => TRUE);
    $options['inherit_title'] = array('default' => FALSE, 'bool' => TRUE);
    $options['inherit_breadcrumbs'] = array('default' => FALSE, 'bool' => TRUE);
    $options['inherit_first_row_tokens'] = array('default' => FALSE, 'bool' => TRUE);
    return $options;
  }

  /**
   * Default options form that provides the label widget that all fields
   * should have.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    $view_display = $this->view->name . ':' . $this->view->current_display;

    $options = array('' => t('-Select-'));
    $options += views_get_views_as_options(FALSE, 'all', $view_display, FALSE, TRUE);
    $form['view_to_insert'] = array(
      '#type' => 'select',
      '#title' => t('View to insert'),
      '#default_value' => $this->options['view_to_insert'],
      '#description' => t('The view to insert into this area.'),
      '#options' => $options,
    );

    $form['inherit_arguments'] = array(
      '#type' => 'checkbox',
      '#title' => t('Inherit contextual filters'),
      '#default_value' => $this->options['inherit_arguments'],
      '#description' => t('If checked, this view will receive the same contextual filters as its parent.'),
    );
    $form['inherit_title'] = array(
      '#type' => 'checkbox',
      '#title' => t('Inherit title'),
      '#default_value' => $this->options['inherit_title'],
      '#description' => t('If checked, this view will pass its title to its parent using any simple argument replacement in this view.'),
      '#dependency' => array(
        'edit-options-inherit-arguments' => array(1),
      ),
    );
    $form['inherit_breadcrumbs'] = array(
      '#type' => 'checkbox',
      '#title' => t('Inherit breadcrumbs'),
      '#default_value' => $this->options['inherit_breadcrumbs'],
      '#description' => t('If checked, this view will pass any breadcrumbs to its parent using any simple argument replacement in this view. WARNING this affects performance as it executes the view one extra time. Only use this if you need to get token values from the view into the title!'),
      '#dependency' => array(
        'edit-options-inherit-arguments' => array(1),
      ),
    );
    $form['inherit_first_row_tokens'] = array(
      '#type' => 'checkbox',
      '#title' => t('Inherit title with first row token replacements'),
      '#default_value' => $this->options['inherit_first_row_tokens'],
      '#description' => t('If checked, this view will get any first row tokens replaced. WARNING this affects performance as it executes the view one extra time. Only use this if you need to get token values from the view into the title!'),
      '#dependency' => array(
        'edit-options-inherit-title' => array(1),
      ),
    );
  }

  /**
   * Render the area
   */
  function render($empty = FALSE) {
    if (!empty($this->options['view_to_insert'])) {
      list($view_name, $display_id) = explode(':', $this->options['view_to_insert']);

      $view = views_get_view($view_name);
      if (empty($view) || !$view->access($display_id)) {
        return;
      }
      $view->set_display($display_id);

      // Avoid recursion
      $view->parent_views += $this->view->parent_views;
      $view->parent_views[] = "$view_name:$display_id";

      // If we need to do token replacements from the first row for the title (or at all???).
      if (!empty($this->options['inherit_first_row_tokens'])) {
        $view_copy = views_get_view($view_name);
        if (empty($view_copy) || !$view_copy->access($display_id)) {
          return;
        }
        $view_copy->set_display($display_id);

        // Avoid recursion
        $view_copy->parent_views += $this->view->parent_views;
        $view_copy->parent_views[] = "$view_name:$display_id";
      }

      // Check if the view is part of the parent views of this view
      $search = "$view_name:$display_id";
      if (in_array($search, $this->view->parent_views)) {
        drupal_set_message(t("Recursion detected in view @view display @display.", array('@view' => $view_name, '@display' => $display_id)), 'error');
      }
      else {
        if (!empty($this->options['inherit_arguments']) && !empty($this->view->args)) {
          if (!empty($this->options['inherit_first_row_tokens']) || !empty($this->options['inherit_breadcrumbs'])) {
            if (!empty($this->view->args)) {
              $view_copy->pre_execute($this->view->args);
              $view_copy->execute();
              $vc_result = $view_copy->result;
              $view_copy->destroy();
              if ($vc_result) {
                $view->pre_execute($this->view->args);
                $view->display_handler->execute();
                $view->get_breadcrumb();
                $this->view->set_title($view->get_title());
                if (!empty($this->options['inherit_breadcrumbs'])) {
                  $this->view->build_info['breadcrumb'] = $view->build_info['breadcrumb'];
                }
              }
            }
          }
          elseif (!empty($this->options['inherit_title']) && !empty($this->view->args)) {
            $view->pre_execute($this->view->args);
            $view->execute();
            $this->view->set_title($view->get_title());
          }
          return $view->preview($display_id, $this->view->args);
        }
        else {
          return $view->preview($display_id);
        }
      }
    }
    return '';
  }
}
