<?php


function views_area_view_with_more_views_data_alter(&$data) {
  $data['views']['view_with_more'] = array(
     'title' => t('View area with more'),
     'help' => t('Insert a view inside an area, but with more.'),
     'area' => array(
       'handler' => 'views_handler_area_view_with_more',
    ),
  );
}